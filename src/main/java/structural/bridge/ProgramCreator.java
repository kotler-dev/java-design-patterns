package structural.bridge;

import java.util.ArrayList;

public class ProgramCreator {
    public static void main(String[] args) {
        Program[] programs = {
                new StockExchange(new JavaDeveloper()),
                new StockExchange(new CppDeveloper()),
                new BankSystem(new JavaDeveloper()),
                new BankSystem(new CppDeveloper())
        };

        for (Program program : programs) {
            program.developProgram();
        }
    }
}
