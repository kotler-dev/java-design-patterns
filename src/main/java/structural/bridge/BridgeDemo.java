package structural.bridge;

// https://en.wikipedia.org/wiki/Bridge_pattern

interface Logger {
    static Logger info() {
        return message -> System.out.println("[Info] " + message);
    }

    static Logger warning() {
        return message -> System.out.println("[Warning] " + message);
    }

    void log(String message);
}

abstract class AbstractAccount {
    private Logger logger = Logger.info();

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    protected void operate(String message, boolean result, int balance) {
        logger.log(message + ", " + result + ", " + balance);
    }
}

class SimpleAccount extends AbstractAccount {
    private int balance;

    public SimpleAccount(int balance) {
        this.balance = balance;
    }

    public  boolean isBalance() {
        return balance < 50;
    }

    public void withDraw(int amount) {
        boolean shouldPerform = balance >= amount;

        if (shouldPerform) {
            balance -= amount;
        }
        operate("withDraw " + amount, shouldPerform, balance);
    }
}

public class BridgeDemo {
    public static void main(String[] args) {
        SimpleAccount account = new SimpleAccount(100);
        account.withDraw(75);

        if (account.isBalance()) {
            account.setLogger(Logger.warning());
        }

        account.withDraw(10);
        account.withDraw(100);
    }
}
