package structural.adapter;

public class DatabaseRunner {
    public static void main(String[] args) {
        DataBase dataBase = new AdapterCustomToDatabase();
        dataBase.insert();
        dataBase.select();
        dataBase.update();
        dataBase.delete();
    }
}
