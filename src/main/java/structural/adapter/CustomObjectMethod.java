package structural.adapter;

public class CustomObjectMethod {
    void insertCustomObjectMethod() {
        System.out.println("CustomObjectMethod -> Insert");
    }

    void selectCustomObjectMethod() {
        System.out.println("CustomObjectMethod -> Select");
    }

    void updateCustomObjectMethod() {
        System.out.println("CustomObjectMethod -> Update");
    }

    void deleteCustomObjectMethod() {
        System.out.println("CustomObjectMethod -> Delete");
    }
}
