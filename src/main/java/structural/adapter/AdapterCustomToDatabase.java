package structural.adapter;

public class AdapterCustomToDatabase extends CustomObjectMethod implements DataBase{
    @Override
    public void insert() {
        insertCustomObjectMethod();
    }

    @Override
    public void select() {
        selectCustomObjectMethod();
    }

    @Override
    public void update() {
        updateCustomObjectMethod();
    }

    @Override
    public void delete() {
        deleteCustomObjectMethod();
    }
}
