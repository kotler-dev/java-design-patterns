package structural.adapter;

public interface DataBase {
    void insert();
    void select();
    void update();
    void delete();
}
